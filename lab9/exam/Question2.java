package exam;

public class Question2 {

	public static void main(String[] args) {
        System.out.println(minV1(10, 6, 4));
	}
	
	private static int minV1(int a, int b, int c) {
		int min = a < b ? a : b;
		return min < c ? min : c;
	}

}
